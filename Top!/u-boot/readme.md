[hexdump, Posted April 5](https://forum.armbian.com/topic/13611-infrastructure-needed-for-releasing-new-kernels-for-armbian-for-tv-boxes/?tab=comments#comment-98556)
@jock - that approach with erasing the emmc might work quite well for some boxes, but not for others: i have an rk3318 box where i was not able to get mainline u-boot+atf running at all and was relying on the first stage of the original bootloader ... there are allwinner boxes where mainline u-boot is not able to get the memory timing right - the only option there is to build a hacked non-redistributable u-boot using an allwinner blob and on amlogic it is even more complex: here u-boot uses binary blobs for the memory timing etc. which are often specific to a certain box, so that you'll have to first extract the original boot blocks from the box, extract those sections from it and and build a mainline u-boot with them following the for sure not trivial build procedure for the amlogic u-boot - it can be done if one is used to doing such things but i think it will fail as a generic solution for everyone ...

best wishes - hexdump



[balbes150, Posted April 10]  
   On 4/10/2020 at 3:29 AM, Igor said:
not changing boot loaders, which prevents making standards.

Consider u-boot as a BIOS on a PC and everything falls into place, you do not need to interfere with its operation. :) No one in the field of development of Debian|Ubuntu and other distributions for PC does not develop BIOS for PC, this is a direct task of the manufacturer, who knows all the features of their hardware. Similarly to u-boot for TV boxes, let the manufacturer deal with it, our task is to add to it the minimum means of starting any system absolutely and, if possible, installing the system in internal memory (eMMC). The main u-boot will never be able to work fully on all TV boxes.